Rails.application.configure do
  # host = ENV.fetch('HOST', 'localhost')
  # port = ENV.fetch('PORT', '3000')
  # config.action_cable.allowed_request_origins = ["http://#{host}:#{port}", "https://#{host}:#{port}"]

  # Disable protection and allow requests from any origin
  config.action_cable.disable_request_forgery_protection = true
end
