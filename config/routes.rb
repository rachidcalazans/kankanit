Rails.application.routes.draw do
  devise_for :users, controllers: {
                                    sessions: 'users/sessions',
                                    registrations: 'users/registrations',
                                    passwords: 'users/passwords'
                                  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  mount ActionCable.server => '/cable'

  devise_scope :user do
    root to: "users/sessions#new"
  end

  resources :projects, only: [:index]
  resources :sprints, only: [:index]
  resources :session, only: [:index]
  resources :tasks, only: [:index, :update] do
    get :task_description
    post :play
    post :pause
    post :transfer
    post :add_comment
  end
end
