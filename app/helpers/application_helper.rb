module ApplicationHelper
  def bootstrap_class_for(flash_type)
    case flash_type
    when "success"
      "alert-success"   # Green
    when "error"
      "alert-danger"    # Red
    when "alert"
      "alert-warning"   # Yellow
    when "notice"
      "alert-info"      # Blue
    else
      flash_type.to_s
    end
  end

  def draw_error_div(attribute)
    raw "<div class='#{attribute}-error-msg error-msg'></div>"
  end

  # Add the error msg on correctly div of each attribute
  # Param :model_name => to identify which object belongs
  # Param :errors     => all errors to be shown
  def raw_messages_errors(model_name, errors)
    js_errors = String.new

    errors.each do |full_attribute, errors_array|
      id        = String.new
      objects   = full_attribute.to_s.split('.')
      attribute = objects.last
      objects.pop #drop the last element

      id << "##{model_name}_"
      objects.each do |object|
        id << "#{object}_attributes_"
      end
      id << "#{attribute}"
      # js_errors << "$('#{id}').addClass('error-input');".html_safe
      js_errors << "$('.#{full_attribute}-error-msg').html(\"#{errors_array}\");".html_safe
    end
    raw  js_errors
  end

end
