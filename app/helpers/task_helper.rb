module TaskHelper

  def get_responsible_from_team_users(responsible_id, team_users)
    team_users.find { |user| user.id == responsible_id }
  end

end
