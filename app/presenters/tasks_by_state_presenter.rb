class TasksByStatePresenter

  attr_reader :tasks, :on_production

  def initialize tasks, tasks_on_production
    @tasks         = tasks
    @on_production = tasks_on_production
  end

  def todo
    @todo ||= tasks_by [:to_do, :disapproved_on_stage, :disapproved_on_review]
  end

  def in_progress
    @in_progress ||= tasks_by [:doing]
  end

  def in_review
    @in_review ||= tasks_by [:on_review]
  end

  def approved_in_review
    @approved_in_review ||= tasks_by [:approved_on_review]
  end

  def on_stage
    @on_stage ||= tasks_by [:on_stage]
  end

  def approved_on_stage
    @approved_on_stage ||= tasks_by [:approved_on_stage]
  end

  def cloned_to_next_sprint
    @cloned_to_next_sprint ||= tasks_by [:cloned_to_next_sprint]
  end

  # def on_production
  #   @on_production ||= tasks_by [:delivered, :on_production]
  # end

  private

  def tasks_by states
    tasks.select { |task| states.include?(task.task_state) }
  end

end
