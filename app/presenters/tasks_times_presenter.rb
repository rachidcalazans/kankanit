class TasksTimesPresenter

  private
  attr_reader :tasks, :delivered_tasks,
    :estimated_time, :worked_time

  public
  def initialize tasks, delivered_tasks
    @tasks           = tasks
    @delivered_tasks = delivered_tasks

    @estimated_time = 0
    @worked_time    = 0

    calculate_times
  end

  def estimated_time_in_hours
    time_in_hours estimated_time
  end

  def worked_time_in_hours
    time_in_hours worked_time
  end

  private
  def calculate_times
    add_times_to tasks
    add_times_to delivered_tasks
  end

  def add_times_to(tasks_collection)
    tasks_collection.each do |task|
      add_estimated_time task.current_estimate_seconds
      add_worked_time task.current_worked_time
    end
  end

  def add_estimated_time seconds = 0
    @estimated_time += seconds
  end

  def add_worked_time seconds = 0
    @worked_time += seconds
  end

  def time_in_hours time
    time / 60 / 60
  end

end
