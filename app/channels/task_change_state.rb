class TaskChangeState < ApplicationCable::Channel

  def subscribed
    stream_from 'task_change_state'
  end

end
