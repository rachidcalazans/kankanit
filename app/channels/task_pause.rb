class TaskPause < ApplicationCable::Channel

  def subscribed
    stream_from 'task_pause'
  end

end

