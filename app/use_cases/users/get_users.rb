module UseCases
  module Users
    class GetUsers

      private
      attr_reader :user_repository

      public
      def initialize user_repository
        @user_repository = user_repository
      end

      def users(user_ids)
        user_repository.all_by_ids(user_ids)
      end

    end
  end
end
