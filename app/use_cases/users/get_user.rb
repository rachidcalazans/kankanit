module UseCases
  module Users
    class GetUser

      private
      attr_reader :user_repository

      public
      def initialize user_repository
        @user_repository = user_repository
      end

      def user_by_email(email)
        user_repository.find_by_email(email)
      end

    end
  end
end
