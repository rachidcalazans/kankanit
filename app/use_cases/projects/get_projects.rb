module UseCases
  module Projects
    class GetProjects

      private
      attr_reader :project_repository

      public
      def initialize project_repository
        @project_repository = project_repository
      end

      def projects(parameters, callback)
        begin
          user = parameters[:user]
          filtered_by_user(user) do |projects|
            callback[:on_success].call projects
          end
        rescue => e
          callback[:on_error].call e
        end
      end

      private
      def filtered_by_user(user, &block)
        if user.is_master_or_manager?
          project_repository.all(&block)
        else
          project_repository.all(&block)
          #project_repository.all_by_user(user.id, &block)
        end
      end

    end
  end
end
