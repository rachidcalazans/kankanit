module UseCases
  module Projects
    class GetProject

      private
      attr_reader :project_repository

      public
      def initialize project_repository
        @project_repository = project_repository
      end

      def project project_id
        @project_repository.find(project_id)
      end

    end
  end
end
