module UseCases
  module Sprints
    class GetSprint

      private
      attr_reader :sprint_repository

      public
      def initialize sprint_repository
        @sprint_repository = sprint_repository
      end

      def sprint(sprint_id)
        sprint_repository.find(sprint_id)
      end

    end
  end
end
