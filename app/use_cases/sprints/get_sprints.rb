module UseCases
  module Sprints
    class GetSprints

      private
      attr_reader :sprint_repository

      public
      def initialize sprint_repository
        @sprint_repository = sprint_repository
      end

      def sprints(parameters, callback)
        begin
          user       = parameters[:user]
          project_id = parameters[:project_id]

          filtered_by_user(user: user, project_id: project_id) do |sprints|
            callback[:on_success].call sprints
          end
        rescue => e
          callback[:on_error].call e
        end
      end

      private
      def filtered_by_user(user:, project_id:, &block)
        if user.is_master_or_manager?
          sprint_repository.all_by_project(project_id: project_id, &block)
        else
          sprint_repository.all_by_project(project_id: project_id, &block)
          #sprint_repository.all_by_project_and_user(project_id: project_id, user_id: user.id, &block)
        end
      end

    end
  end
end
