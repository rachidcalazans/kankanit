module UseCases
  module Teams
    class GetTeam

      private
      attr_reader :team_repository

      public
      def initialize team_repository
        @team_repository = team_repository
      end

      def team(parameters, callback)
        begin
          team_id = parameters[:team_id]
          return unless team_id.present?

          team_repository.find(team_id: team_id) do |team|
            callback[:on_success].call(team)
          end
        rescue => e
          callback[:on_error].call e
        end
      end

    end
  end
end
