module UseCases
  module Tasks
    class UpdateTask

      private
      attr_reader :task_repository

      public
      def initialize task_repository
        @task_repository = task_repository
      end

      def change_state parameters, callback
        begin
          task_id       = parameters[:task_id]
          task_state_id = Domain::Enum::TaskState.value_by(parameters[:task_state_sym])
          task_repository.change_state(task_id: task_id, task_state_id: task_state_id) do |task|
            callback[:on_success].call(task)
          end
        rescue => e
          callback[:on_error].call e
        end
      end

    end
  end
end
