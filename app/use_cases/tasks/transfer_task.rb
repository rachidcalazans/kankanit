module UseCases
  class TransferTask

    private
    attr_reader :task_repository

    public
    def initialize task_repository
      @task_repository = task_repository
    end

    def transfer parameters, callback
      begin
        task_id        = parameters[:task_id]
        responsible_id = parameters[:responsible_id]
        team_id        = parameters[:team_id]
        task_repository.transfer(task_id, responsible_id, team_id) do |task|
          callback[:on_success].call(task)
        end
      rescue => e
        callback[:on_error].call e
      end
    end

  end
end
