module UseCases
  module Tasks
    class GetComments

      private
      attr_reader :comment_repository

      public
      def initialize(comment_repository)
        @comment_repository = comment_repository
      end

      def all_by_task parameters, callback
        begin
          task_id = parameters[:task_id]

          comment_repository.all_by_task(task_id: task_id) do |comments|
            callback[:on_success].call(comments)
          end
        rescue => e
          callback[:on_error].call e
        end
      end

    end
  end
end
