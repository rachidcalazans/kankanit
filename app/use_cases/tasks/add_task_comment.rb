module UseCases
  module Tasks
    class AddTaskComment

      private
      attr_reader :comment_repository

      public
      def initialize comment_repository
        @comment_repository = comment_repository
      end

      def add_comment parameters, callback
        begin
          task_id = parameters[:task_id]
          text    = parameters[:text]
          comment_repository.add_comment(task_id: task_id, text: text) do |comment|
            callback[:on_success].call(comment)
          end
        rescue => e
          callback[:on_error].call e
        end
      end

    end
  end
end
