module UseCases
  module Tasks
    class GetTasks

      private
      attr_reader :task_repository

      public
      def initialize task_repository
        @task_repository = task_repository
      end

      def tasks parameters, callback
        begin
          user_id   = parameters[:user_id]
          sprint_id = parameters[:sprint_id]

          if user_id
            all_by_sprint_and_user(sprint_id, user_id, callback[:on_success])
          else
            all_by_sprint(sprint_id, callback[:on_success])
          end

        rescue => e
          callback[:on_error].call e
        end
      end

      private
      def all_by_sprint_and_user(sprint_id, user_id, on_success_callback)
        task_repository.all_by_sprint_and_user(sprint_id, user_id) do |tasks|
          task_repository.all_delivered_by_sprint_and_user(sprint_id, user_id) do |delivered_tasks|
            on_success_callback.call(tasks, delivered_tasks)
          end
        end
      end

      def all_by_sprint(sprint_id, on_success_callback)
        task_repository.all_by_sprint(sprint_id) do |tasks|
          task_repository.all_delivered_by_sprint(sprint_id) do |delivered_tasks|
            on_success_callback.call(tasks, delivered_tasks)
          end
        end
      end

    end
  end
end
