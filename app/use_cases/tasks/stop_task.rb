module UseCases
  module Tasks
    class StopTask

      private
      attr_reader :task_repository

      public
      def initialize task_repository
        @task_repository = task_repository
      end

      def stop parameters, callback
        begin
          task_id = parameters[:task_id]
          task_repository.stop(task_id) do |task|
            callback[:on_success].call(task)
          end
        rescue => e
          callback[:on_error].call e
        end
      end

    end
  end
end
