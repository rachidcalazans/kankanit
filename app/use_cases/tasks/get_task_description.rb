module UseCases
  module Tasks
    class GetTaskDescription

      private
      attr_reader :task_repository

      public
      def initialize task_repository
        @task_repository = task_repository
      end

      def find_task_description parameters, callback
        begin
          task_id = parameters[:task_id]
          task_repository.find_task_description(task_id: task_id) do |task_description|
            callback[:on_success].call(task_description)
          end
        rescue => e
          callback[:on_error].call e
        end
      end

    end
  end
end
