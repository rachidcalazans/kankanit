class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  protect_from_forgery with: :exception

  helper_method :logged_user

  def after_sign_in_path_for(resource)
    projects_path
  end

  def logged_user
    @logged_user ||= Domain::Entity::User.new(user_session['user']) if user_session
  end

end
