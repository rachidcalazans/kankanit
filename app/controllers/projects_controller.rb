class ProjectsController < ApplicationController
  private
  attr_reader :get_projects

  before_action :provide_get_projects

  def provide_get_projects
    @get_projects = ::Injections::UseCase.provide_get_projects(logged_user)
  end

  public
  def index
    parameters = {user: logged_user}

    get_projects.projects(parameters, {
      on_success: -> (projects) {
        @projects = projects
        flash[:alert] = nil
      },
      on_error: -> (error) {
        @projects = []
        flash[:alert] = 'Some error happened ' + error.message
      }
    })

  end

end

