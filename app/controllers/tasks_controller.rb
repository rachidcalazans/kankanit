class TasksController < ApplicationController
  private
  attr_reader :get_project, :get_sprint, :get_tasks, :start_task,
    :stop_task, :transfer_task, :update_task,
    :get_task_description, :get_comments, :add_task_comment,
    :get_users, :get_team

  before_action :provide_get_project,          only: [:index]
  before_action :provide_get_sprint,           only: [:index]
  before_action :provide_get_tasks,            only: [:index]
  before_action :provide_get_users,            only: [:index]
  before_action :provide_get_team,             only: [:index]
  before_action :provide_update_task,          only: [:update]
  before_action :provide_start_task,           only: [:play]
  before_action :provide_stop_task,            only: [:pause]
  before_action :provide_transfer_task,        only: [:transfer]
  before_action :provide_get_task_description, only: [:task_description]
  before_action :provide_get_comments,         only: [:task_description]
  before_action :provide_add_task_comment,     only: [:add_comment]

  def provide_get_project
    @get_project = ::Injections::UseCase.provide_get_project(logged_user)
  end
  
  def provide_get_sprint
    @get_sprint = ::Injections::UseCase.provide_get_sprint(logged_user)
  end

  def provide_get_tasks
    @get_tasks = ::Injections::UseCase.provide_get_tasks(logged_user)
  end

  def provide_get_users
    @get_users = ::Injections::UseCase.provide_get_users(logged_user)
  end

  def provide_get_team
    @get_team = ::Injections::UseCase.provide_get_team(logged_user)
  end

  def provide_update_task
    @update_task = ::Injections::UseCase.provide_update_task(logged_user)
  end

  def provide_start_task
    @start_task = ::Injections::UseCase.provide_start_task(logged_user)
  end

  def provide_stop_task
    @stop_task = ::Injections::UseCase.provide_stop_task(logged_user)
  end

  def provide_transfer_task
    @transfer_task = ::Injections::UseCase.provide_transfer_task(logged_user)
  end

  def provide_get_task_description
    @get_task_description = ::Injections::UseCase.provide_get_task_description(logged_user)
  end

  def provide_get_comments
    @get_comments = ::Injections::UseCase.provide_get_comments(logged_user)
  end

  def provide_add_task_comment
    @add_task_comment = ::Injections::UseCase.provide_add_task_comment(logged_user)
  end

  public
  def index
    user_id   = params[:user_id]
    sprint_id = params[:sprint_id].to_i
    @sprint   = get_sprint.sprint(sprint_id)
    @project_name = get_project.project(@sprint.project_id).name

    get_tasks.tasks({user_id: user_id, sprint_id: sprint_id}, {
      on_success: -> (tasks, delivered_tasks) {
        @total_of_delivered_tasks = delivered_tasks.size
        @total_of_tasks           = tasks.size + @total_of_delivered_tasks
        @tasks_times              = TasksTimesPresenter.new(tasks, delivered_tasks)
        @tasks_by_state           = TasksByStatePresenter.new tasks, delivered_tasks

        team_id = tasks.present? ? tasks.first.team_id : nil

        @team_users = []
        get_team.team({team_id: team_id}, {
          on_success: -> (team) {
            @team_users = get_users.users(team.user_ids)
          },
          on_error: -> (error) { raise error }
        })

        flash[:alert] = nil
      },
      on_error: -> (error) { raise error }
    })
  end

  def task_description
    @task_id = params[:id]

    get_task_description.find_task_description({task_id: @task_id}, {
      on_success: -> (task_description)  {
        @task_description = task_description
      },
      on_error:   -> (error) { raise error }
    })

    get_comments.all_by_task({task_id: @task_id}, {
      on_success: -> (comments)  {
        @task_comments = comments
      },
      on_error:   -> (error) { raise error }
    })

  end

  def update
    task_id = params[:id]
    task_state_sym = params['task']['task_state_sym'].to_sym
    parameters = {task_id: task_id, task_state_sym: task_state_sym}
    update_task.change_state(parameters, {
      on_success: -> (task) {
        ActionCable.server.broadcast 'task_change_state',
        user_id: task.responsible_id,
        task_id: task_id,
        task_state: task_state_sym
      },
      on_error: -> (error) { raise error }
    })
  end

  def play
    task_id = params[:id]
    start_task.start({task_id: task_id}, {
      on_success: -> (task)  { ActionCable.server.broadcast('task_play', task_id: task.id) },
      on_error:   -> (error) { raise error }
    })
  end

  def pause
    task_id = params[:id]
    task_id = params[:id]
    stop_task.stop({task_id: task_id}, {
      on_success: -> (task)  { ActionCable.server.broadcast('task_pause', task_id: task.id) },
      on_error:   -> (error) { raise error }
    })
  end

  def transfer
    task_id        = params[:task_id]
    responsible_id = params[:responsible_id]
    team_id        = params[:team_id]
    parameters = {task_id: task_id, responsible_id: responsible_id, team_id: team_id}

    transfer_task.transfer(parameters, {
      on_success: -> (task)  {
        @task_id        = task.id
        @responsible_id = task.responsible_id
      },
      on_error:   -> (error) { raise error }
    })
  end

  def add_comment
    @task_id = params[:task_id]
    text     = params[:text]

    parameters = {task_id: @task_id, text: text}

    add_task_comment.add_comment(parameters, {
      on_success: -> (comment)  {
        @comment = comment
      },
      on_error:   -> (error) { raise error }
    })
  end

end
