class Users::RegistrationsController < Devise::RegistrationsController
  layout 'signup'
  before_filter :configure_permitted_parameters

  def create
    user = nil
    super do |resource|
      if resource.valid?
        get_user   = Injections::UseCase.provide_get_user(resource)
        user = get_user.user_by_email(resource.email)
        if user
          user.user_token = resource.user_token
        else
          user = Domain::Entity::User.new(user_token: resource.user_token)
        end
      end
    end
    user_session['user'] = user if user
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:user_token])
  end

end
