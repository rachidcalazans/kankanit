class Users::SessionsController < Devise::SessionsController
  layout 'login'

  def create
    user = nil
    super do |resource|
      get_user   = Injections::UseCase.provide_get_user(resource)
      user = get_user.user_by_email(resource.email)
      if user
        user.user_token = resource.user_token
      else
        user = Domain::Entity::User.new(user_token: resource.user_token)
      end
    end

    user_session['user'] = user
  end

end
