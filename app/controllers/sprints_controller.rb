class SprintsController < ApplicationController

  private
  attr_reader :get_project, :get_sprints

  before_action :provide_get_project, only: [:index]
  before_action :provide_get_sprints, only: [:index]

  def provide_get_project
    @get_project = ::Injections::UseCase.provide_get_project(logged_user)
  end

  def provide_get_sprints
    @get_sprints = ::Injections::UseCase.provide_get_sprints(logged_user)
  end

  public
  def index
    return @sprints = [] unless logged_user.present?

    project_id = params[:project_id].to_i
    @project_name = get_project.project(project_id).name

    parameters = {project_id: project_id, user: logged_user}

    get_sprints.sprints(parameters, {
      on_success: -> (sprints) {
        @sprints = sprints
        flash[:alert] = nil
      },
      on_error: -> (error) {
        @sprints = []
        flash[:alert] = 'Some error happened ' + error.message
      }
    })
  end

end
