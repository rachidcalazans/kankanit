$(document).on 'turbolinks:load', ->

  $('.project-card').on 'click', ->
    project_id = $(this).data('project-id')
    url = '/sprints?project_id=' + project_id
    window.location.href = url
    return
