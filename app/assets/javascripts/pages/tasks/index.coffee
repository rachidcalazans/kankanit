$(document).on 'turbolinks:load', ->

  $('.task-play').on 'click', () ->
    task_id    = this.dataset.taskId
    task_title = this.dataset.taskTitle
    $.ajax
      url: '/tasks/' + task_id + '/play'
      method: 'POST'
      context: document.body
      data: id: task_id
      success: ->
        # $('#btn-task-play-' + task_id).toggle('hidden')
        # $('#btn-task-pause-' + task_id).toggle('hidden')
        # $('#' + task_id).removeClass('card-user-paused')
        # $('#' + task_id).addClass('card-user-played')



  $('.task-pause').on 'click', () ->
    task_id    = this.dataset.taskId
    task_title = this.dataset.taskId
    $.ajax
      url: '/tasks/' + task_id + '/pause'
      method: 'POST'
      context: document.body
      data: id: task_id
      success: ->
        # $('#btn-task-play-' + task_id).toggle('hidden')
        # $('#btn-task-pause-' + task_id).toggle('hidden')
        # $('#' + task_id).removeClass('card-user-played')
        # $('#' + task_id).addClass('card-user-paused')

  $('.card > .card-click-details').on 'click', () ->
    task_id = this.dataset.taskId
    $.ajax
      url: '/tasks/' + task_id + '/task_description'
      method: 'GET'
      context: document.body
      data: id: task_id

  $('.card').bind 'dragstart', (event) ->
    event.originalEvent.dataTransfer.setData 'text/plain', event.target.getAttribute('id')
    return

  # bind the dragover event on the board sections
  $('.board').bind 'dragover', (event) ->
    event.preventDefault()
    return

  # bind the drop event on the board sections
  $('.boards > .board').bind 'drop', (event) ->
    notecard = event.originalEvent.dataTransfer.getData('text/plain')
    task_status_sym = event.target.closest('.board').dataset.taskStatusSym
    event.preventDefault()
    $.ajax
      url: '/tasks/' + notecard
      method: 'PUT'
      context: document.body
      data: task: task_state_sym: task_status_sym
      success: ->
        # event.target.closest('.board').appendChild document.getElementById(notecard)
        # event.preventDefault()
        return
    return
