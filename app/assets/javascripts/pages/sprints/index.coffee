$(document).on 'turbolinks:load', ->

  $('.sprint-card').on 'click', ->
    sprint_id = $(this).data('sprint-id')
    url = '/tasks?sprint_id=' + sprint_id
    window.location.href = url
    return
