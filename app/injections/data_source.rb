module Injections
  class DataSource

    def self.provide_user_remote_data_source(user)
      header   = {
        content_type: 'application/json',
        'App-Key':    '35d66012e4e564d15dfe276f30f1532d',
        'User-Token': user.user_token
      }

      rest_client = RestClient::Resource.new(REMOTE_BASE_URL, headers: header)
      Infrastructure::Remote::User::RunRunIt::UserRemoteDataSource.new(rest_client)
    end

    def self.provide_project_remote_data_source(user)
      header   = {
        content_type: 'application/json',
        'App-Key':    '35d66012e4e564d15dfe276f30f1532d',
        'User-Token': user.user_token
      }

      rest_client = RestClient::Resource.new(REMOTE_BASE_URL, headers: header)
      Infrastructure::Remote::Project::RunRunIt::ProjectRemoteDataSource.new(rest_client)
    end

    def self.provide_sprint_remote_data_source(user)
      header   = {
        content_type: 'application/json',
        'App-Key':    '35d66012e4e564d15dfe276f30f1532d',
        'User-Token': user.user_token
      }

      rest_client = RestClient::Resource.new(REMOTE_BASE_URL, headers: header)
      Infrastructure::Remote::Sprint::RunRunIt::SprintRemoteDataSource.new(rest_client)
    end

    def self.provide_task_remote_data_source(user)
      header   = {
        content_type: 'application/json',
        'App-Key':    '35d66012e4e564d15dfe276f30f1532d',
        'User-Token': user.user_token
      }

      rest_client = RestClient::Resource.new(REMOTE_BASE_URL, headers: header)
      ::Infrastructure::Remote::Task::RunRunIt::TaskRemoteDataSource.new(rest_client)
     end

     def self.provide_comment_remote_data_source(user)
      header   = {
        content_type: 'application/json',
        'App-Key':    '35d66012e4e564d15dfe276f30f1532d',
        'User-Token': user.user_token
      }

      rest_client = RestClient::Resource.new(REMOTE_BASE_URL, headers: header)
      ::Infrastructure::Remote::Task::RunRunIt::CommentRemoteDataSource.new(rest_client)
     end

     def self.provide_team_remote_data_source(user)
      header   = {
        content_type: 'application/json',
        'App-Key':    '35d66012e4e564d15dfe276f30f1532d',
        'User-Token': user.user_token
      }

      rest_client = RestClient::Resource.new(REMOTE_BASE_URL, headers: header)
      ::Infrastructure::Remote::Team::RunRunIt::TeamRemoteDataSource.new(rest_client)
     end

  end
end
