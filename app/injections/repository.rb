module Injections
  class Repository

    def self.provide_user_repository(user)
      user_remote_data_source = DataSource.provide_user_remote_data_source(user)
      Domain::Repository::User::UserRepository
        .new(user_remote_data_source: user_remote_data_source, user_local_data_source: ::User)
    end

    def self.provide_project_repository(user)
      remote_data_source = DataSource.provide_project_remote_data_source(user)
      Domain::Repository::Project::ProjectRepository.new(remote_data_source, provide_task_repository(user))
    end

    def self.provide_sprint_repository(user)
      sprint_remote_data_source = DataSource.provide_sprint_remote_data_source(user)
      Domain::Repository::Sprint::SprintRepository.new(sprint_remote_data_source, provide_task_repository(user))
    end

    def self.provide_task_repository(user)
      remote_data_source = DataSource.provide_task_remote_data_source(user)
      Domain::Repository::Task::TaskRepository.new(remote_data_source)
    end

    def self.provide_comment_repository(user)
      remote_data_source = DataSource.provide_comment_remote_data_source(user)
      Domain::Repository::Task::CommentRepository.new(remote_data_source)
    end

    def self.provide_team_repository(user)
      remote_data_source = DataSource.provide_team_remote_data_source(user)
      Domain::Repository::Team::TeamRepository.new(remote_data_source)
    end

  end
end
