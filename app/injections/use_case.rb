module Injections
  class UseCase

    def self.provide_get_user(user)
      user_repository = Injections::Repository.provide_user_repository(user)
      ::UseCases::Users::GetUser.new(user_repository)
    end

    def self.provide_get_users(user)
      user_repository = Injections::Repository.provide_user_repository(user)
      ::UseCases::Users::GetUsers.new(user_repository)
    end

    def self.provide_get_project(user)
      project_repository = Injections::Repository.provide_project_repository(user)
      ::UseCases::Projects::GetProject.new(project_repository)
    end

    def self.provide_get_projects(user)
      project_repository = Injections::Repository.provide_project_repository(user)
      ::UseCases::Projects::GetProjects.new(project_repository)
    end

    def self.provide_get_sprints(user)
      sprint_repository = Injections::Repository.provide_sprint_repository(user)
      ::UseCases::Sprints::GetSprints.new(sprint_repository)
    end

    def self.provide_get_sprint(user)
      sprint_repository = Injections::Repository.provide_sprint_repository(user)
      ::UseCases::Sprints::GetSprint.new(sprint_repository)
    end

    def self.provide_get_team(user)
      team_repository = Injections::Repository.provide_team_repository(user)
      ::UseCases::Teams::GetTeam.new(team_repository)
    end

    def self.provide_get_tasks(user)
      task_repository = Injections::Repository.provide_task_repository(user)
      ::UseCases::Tasks::GetTasks.new(task_repository)
    end

    def self.provide_update_task(user)
      task_repository = Injections::Repository.provide_task_repository(user)
      ::UseCases::Tasks::UpdateTask.new(task_repository)
    end

    def self.provide_start_task(user)
      task_repository = Injections::Repository.provide_task_repository(user)
      ::UseCases::Tasks::StartTask.new(task_repository)
    end

    def self.provide_stop_task(user)
      task_repository = Injections::Repository.provide_task_repository(user)
      ::UseCases::Tasks::StopTask.new(task_repository)
    end

    def self.provide_transfer_task(user)
      task_repository = Injections::Repository.provide_task_repository(user)
      ::UseCases::Tasks::TransferTask.new(task_repository)
    end

    def self.provide_get_task_description(user)
      task_repository = Injections::Repository.provide_task_repository(user)
      ::UseCases::Tasks::GetTaskDescription.new(task_repository)
    end

    def self.provide_get_comments(user)
      comment_repository = Injections::Repository.provide_comment_repository(user)
      ::UseCases::Tasks::GetComments.new(comment_repository)
    end

    def self.provide_add_task_comment(user)
      comment_repository = Injections::Repository.provide_comment_repository(user)
      ::UseCases::Tasks::AddTaskComment.new(comment_repository)
    end

  end
end
