module Domain
  module Repository
    module Sprint
      class SprintRepository
        include SprintDataSource

        private
        attr_reader :sprint_remote_data_source, :task_repository

        public
        def initialize(sprint_remote_data_source, task_repository)
          @sprint_remote_data_source = sprint_remote_data_source
          @task_repository           = task_repository
        end

        def find sprint_id
          sprint_remote_data_source.find(sprint_id)
        end

        def all_by_project(project_id:, &block)
          sprint_remote_data_source.all_by_project(project_id: project_id) do |sprints|
            block.call(sprints)
          end
        end

        def all_by_project_and_user(project_id:, user_id:, &block)
          sprint_remote_data_source.all_by_project(project_id: project_id) do |sprints|
            sprint_ids = sprint_ids_by_user user_id
            sprints_selected = sprints.select { |sprint| sprint_ids.include?(sprint.id) }
            block.call(sprints_selected)
          end
        end

        private
        def sprint_ids_by_user(user_id)
          sprint_ids = []

          task_repository.all_by_user(user_id) do |tasks|
            tasks.each do |task|
              sprint_ids << task.sprint_id unless sprint_ids.include?(task.sprint_id)
            end
          end

          sprint_ids
        end

      end
    end
  end
end
