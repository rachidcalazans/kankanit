module Domain
  module Repository
    module Sprint
      module SprintDataSource

        def find(sprint_id)
          raise NotImplementedError, "This #{self.class} cannot respond :find"
        end

        def all_by_project(project_id:, &block)
          raise NotImplementedError, "This #{self.class} cannot respond :all_by_project"
        end

        def all_by_project_and_user(project_id:, user_id:, &block)
          raise NotImplementedError, "This #{self.class} cannot respond :all_by_project_and_user"
        end

      end
    end
  end
end
