module Domain
  module Repository
    module Project
      module ProjectDataSource

        def find project_id
          raise NotImplementedError, "This #{self.class} cannot respond :find"
        end

        def all &block
          raise NotImplementedError, "This #{self.class} cannot respond :all"
        end

        def all_by_user user_id, &block
          raise NotImplementedError, "This #{self.class} cannot respond :all_by_user"
        end

      end
    end
  end
end
