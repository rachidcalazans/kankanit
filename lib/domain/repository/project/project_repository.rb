module Domain
  module Repository
    module Project
      class ProjectRepository
        include ProjectDataSource

        private
        attr_reader :project_remote_data_source, :task_repository

        public
        def initialize(project_remote_data_source, task_repository)
          @project_remote_data_source = project_remote_data_source
          @task_repository            = task_repository
        end

        def find project_id
          project_remote_data_source.find(project_id)
        end

        def all &block
          project_remote_data_source.all do |projects|
            block.call(projects)
          end
        end

        def all_by_user user_id, &block
          project_remote_data_source.all_by_user(user_id) do |projects|
            sprint_ids = sprint_ids_by_user(user_id)

            selected_projects = projects.select { |project| has_sprint?(project, sprint_ids) }

            block.call(selected_projects)
          end
        end

        private
        def sprint_ids_by_user(user_id)
          sprint_ids = []

          task_repository.all_by_user(user_id) do |tasks|
            tasks.each do |task|
              sprint_ids << task.sprint_id unless sprint_ids.include?(task.sprint_id)
            end
          end

          sprint_ids
        end

        def has_sprint? project, sprint_ids
          project.sprint_ids.each { |sprint_id| return true if sprint_ids.include?(sprint_id) }
          false
        end

      end
    end
  end
end
