module Domain
  module Repository
    module Team
      class TeamRepository
        include TeamDataSource

        private
        attr_reader :team_remote_data_source

        public
        def initialize(team_remote_data_source)
          @team_remote_data_source = team_remote_data_source
        end

        def find(team_id:, &block)
          team_remote_data_source.find(team_id: team_id) do |team|
            block.call(team)
          end
        end

      end
    end
  end
end
