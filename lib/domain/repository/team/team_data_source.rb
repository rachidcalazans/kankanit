module Domain
  module Repository
    module Team
      module TeamDataSource

        def find(team_id:, &block)
          raise NotImplementedError, "This #{self.class} cannot respond :find"
        end

      end
    end
  end
end
