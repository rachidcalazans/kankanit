module Domain
  module Repository
    module User
      module UserDataSource

        def find_by_email(email)
          raise NotImplementedError, "This #{self.class} cannot respond :find_by_email"
        end

        def all_by_ids(user_ids)
          raise NotImplementedError, "This #{self.class} cannot respond :all_by_ids"
        end

      end
    end
  end
end

