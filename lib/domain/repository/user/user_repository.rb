module Domain
  module Repository
    module User
      class UserRepository
        include UserDataSource

        private
        attr_reader :user_remote_data_source, :user_local_data_source

        public
        def initialize(user_remote_data_source:, user_local_data_source:)
          @user_remote_data_source = user_remote_data_source
          @user_local_data_source  = user_local_data_source
        end

        def find_by_email(email)
          user_remote_data_source.find_by_email(email)
        end

        def all_by_ids(user_ids)
          user_remote_data_source.all_by_ids(user_ids)
        end

      end
    end
  end
end
