module Domain
  module Repository
    module Task
      class TaskRepository
        include TaskDataSource

        private
        attr_reader :task_remote_data_source

        public
        def initialize(task_remote_data_source)
          @task_remote_data_source = task_remote_data_source
        end

        def all_by_user user_id, &block
          task_remote_data_source.all_by_user(user_id) do |tasks|
            block.call(tasks)
          end
        end

        def all_by_sprint(sprint_id, &block)
          task_remote_data_source.all_by_sprint(sprint_id) do |tasks|
            block.call(tasks)
          end
        end

         def all_by_sprint_and_user(sprint_id, user_id, &block)
          task_remote_data_source.all_by_sprint_and_user(sprint_id, user_id) do |tasks|
            block.call(tasks)
          end
        end

        def all_delivered_by_sprint(sprint_id, &block)
          task_remote_data_source.all_delivered_by_sprint(sprint_id) do |tasks|
            block.call(tasks)
          end
        end

        def all_delivered_by_sprint_and_user(sprint_id, user_id, &block)
          task_remote_data_source.all_delivered_by_sprint_and_user(sprint_id, user_id) do |tasks|
            block.call(tasks)
          end
        end

        def change_state(task_id:, task_state_id:, &block)
          task_remote_data_source.change_state(task_id: task_id, task_state_id: task_state_id) do |task|
            block.call(task)
          end
        end

        def start task_id, &block
          task_remote_data_source.start(task_id) do |task|
            block.call(task)
          end
        end

        def stop task_id, &block
          task_remote_data_source.stop(task_id) do |task|
            block.call(task)
          end
        end

        def transfer task_id, responsible_id, team_id, &block
          task_remote_data_source.transfer(task_id, responsible_id, team_id) do |task|
            block.call(task)
          end
        end

        def find_task_description(task_id:, &block)
          task_remote_data_source.find_task_description(task_id: task_id) do |task_description|
            block.call(task_description)
          end
        end

      end
    end
  end
end
