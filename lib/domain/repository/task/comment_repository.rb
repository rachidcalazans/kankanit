module Domain
  module Repository
    module Task
      class CommentRepository
        include CommentDataSource

        private
        attr_reader :comment_remote_data_source

        public
        def initialize(comment_remote_data_source)
          @comment_remote_data_source = comment_remote_data_source
        end

        def all_by_task(task_id:, &block)
          comment_remote_data_source.all_by_task(task_id: task_id) do |comments|
            block.call(comments)
          end
        end

        def add_comment(task_id:, text:, &block)
          comment_remote_data_source.add_comment(task_id: task_id, text: text) do |comment|
            block.call(comment)
          end
        end

      end
    end
  end
end
