module Domain
  module Repository
    module Task
      module CommentDataSource

        def all_by_task(task_id:, &block)
          raise NotImplementedError, "This #{self.class} cannot respond :all_by_task"
        end

        def add_comment(task_id:, text:, &block)
          raise NotImplementedError, "This #{self.class} cannot respond :add_comment"
        end

      end
    end
  end
end
