module Domain
  module Repository
    module Task
      module TaskDataSource

        def all_by_user user_id, &block
          raise NotImplementedError, "This #{self.class} cannot respond :all_by_user"
        end

        def all_by_sprint sprint_id, &block
          raise NotImplementedError, "This #{self.class} cannot respond :all_by_sprint"
        end

        def all_by_sprint_and_user sprint_id, user_id, &block
          raise NotImplementedError, "This #{self.class} cannot respond :all_by_sprint_and_user"
        end

        def all_delivered_by_sprint sprint_id, &block
          raise NotImplementedError, "This #{self.class} cannot respond :all_delivered_by_sprint"
        end

        def all_delivered_by_sprint_and_user sprint_id, user_id, &block
          raise NotImplementedError, "This #{self.class} cannot respond :all_delivered_by_sprint_and_user"
        end

        def change_state(task_id:, task_state_id:, &block)
          raise NotImplementedError, "This #{self.class} cannot respond :change_state"
        end

        def start task_id, &block
          raise NotImplementedError, "This #{self.class} cannot respond :start"
        end

        def stop task_id, &block
          raise NotImplementedError, "This #{self.class} cannot respond :stop"
        end

        def transfer task_id, responsible_id, team_id, &block
          raise NotImplementedError, "This #{self.class} cannot respond :transfer"
        end

        def find_task_description(task_id:, &block)
          raise NotImplementedError, "This #{self.class} cannot respond :find_task_description"
        end

      end
    end
  end
end
