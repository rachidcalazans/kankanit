module Domain
  module Enum
    module Base

      def self.included(base)
        base.extend(ClassMethods)
      end

      module ClassMethods

        def hash
          @hash_enum
        end

        def keys
          @hash_enum.keys
        end

        def values
          @hash_enum.values
        end

        def value_by key
          @hash_enum[key]
        end

        def key_by value
          @hash_enum.key(value)
        end

        private
        def hash_enum hash
          @hash_enum = hash
        end

      end
    end
  end
end
