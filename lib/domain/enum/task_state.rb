module Domain
  module Enum
    class TaskState
      include Base

      hash_enum to_do:                 597247,
        doing:                 441432,
        on_review:             502336,
        approved_on_review:    597252,
        disapproved_on_review: 576917,
        on_stage:              597251,
        approved_on_stage:     597257,
        disapproved_on_stage:  597253,
        on_production:         597250,
        delivered:             441433,
        cloned_to_next_sprint: 597751

    end
  end
end
