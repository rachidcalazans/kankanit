module Domain
  module Entity
    class User
      include Base

      attr_reader :id, :name, :email, :avatar_url, :avatar_large_url,
        :is_master, :is_manager

      attr_accessor :user_token

      def is_master_or_manager?
        is_master || is_manager
      end

    end
  end
end
