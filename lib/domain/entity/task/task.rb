module Domain
  module Entity
    module Task
      class Task
        include Base

        attr_reader :id, :uid, :_permission_, :title, :is_working_on,
          :responsible_id, :user_id, :type_id,
          :sprint_id,
          :follower_ids,
          :desired_date, :desired_date_with_time, :estimated_start_date,
          :estimated_delivery_date, :close_date, :priority,
          :task_state_id, :was_reopened, :is_closed,
          :on_going, :team_id, :tag_list,
          :estimated_delivery_date_updated, :queue_position, :scheduled_start_time,
          :created_at, :start_date, :current_estimate_seconds,
          :current_worked_time, :current_evaluator_id, :approved,
          :workflow_id, :repetition_rule_id,
          :checklist_id

        def initialize attrs = {}
          super attrs
        end

        def current_estimate_seconds
          @current_estimate_seconds || 0
        end

        def current_worked_time
          @current_worked_time || 0
        end

        def task_state
          @task_state = Enum::TaskState::key_by(task_state_id)
          @task_state
        end

        def tags
          tag_list.split(',')
        end

        def is_disapproved?
          return true if task_state == :disapproved_on_stage
          return true if task_state == :disapproved_on_review
          false
        end

      end
    end
  end
end
