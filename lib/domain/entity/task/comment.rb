module Domain
  module Entity
    module Task
      class Comment
        include ::Domain::Entity::Base

        attr_reader :id, :user_id, :is_system_message, :text,
          :commenter_name, :children_count, :commentable_id,
          :commentable_type, :created_at, :love_count, :task_id,
          :comment_id, :team_id, :enterprise_id, :document_id

      end
    end
  end
end
