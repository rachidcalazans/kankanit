module Infrastructure
  module RepositoryBase

    private
    attr_reader :rest_client

    public
    def initialize rest_client
      @rest_client = rest_client
    end

    private
    def build_query_params query_params
      return "" unless query_params.present?

      first_clause, first_param = query_params.first
      query = String.new
      query << "?#{first_clause}=#{first_param}"
      query_params.delete(first_clause)

      query_params.each_with_index do |(clause, param), index|
        query << "&#{clause}=#{param}"
      end
      query
    end

  end
end
