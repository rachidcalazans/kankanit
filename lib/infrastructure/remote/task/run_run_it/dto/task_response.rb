module Infrastructure
  module Remote
    module Task
      module RunRunIt
        module Dto
          class TaskResponse
            include Domain::Entity::Base

            attr_reader :id, :uid, :_permission_, :title, :is_working_on,
              :responsible_id, :user_id, :type_id,
              :project_id,
              :follower_ids,
              :desired_date, :desired_date_with_time, :estimated_start_date,
              :estimated_delivery_date, :close_date, :priority,
              :task_state_id, :was_reopened, :is_closed,
              :on_going, :team_id, :tag_list,
              :estimated_delivery_date_updated, :queue_position, :scheduled_start_time,
              :created_at, :start_date, :current_estimate_seconds,
              :current_worked_time, :current_evaluator_id, :approved,
              :workflow_id, :repetition_rule_id,
              :checklist_id


            def as_json
              super.merge(sprint_id: project_id)
            end

          end
        end
      end
    end
  end
end
