module Infrastructure
  module Remote
    module Task
      module RunRunIt
        module Dto
          class TaskDescriptionResponse
            include Domain::Entity::Base

            attr_reader :id, :_permission_, :description

          end
        end
      end
    end
  end
end
