module Infrastructure
  module Remote
    module Task
      module RunRunIt
        class TaskRemoteDataSource
          include RepositoryBase
          include Domain::Repository::Task::TaskDataSource

          def all_by_user user_id, &block
            tasks = tasks(responsible_id: user_id)
            block.call(tasks)
          end

          def all_by_sprint sprint_id, &block
            tasks = tasks(project_id: sprint_id)
            block.call(tasks)
          end

          def all_by_sprint_and_user sprint_id, user_id, &block
            tasks = tasks(project_id: sprint_id, responsible_id: user_id)
            block.call(tasks)
          end

          def all_delivered_by_sprint sprint_id, &block
            tasks = tasks(project_id: sprint_id, is_closed: true)
            block.call(tasks)
          end

          def all_delivered_by_sprint_and_user sprint_id, user_id, &block
            tasks = tasks(project_id: sprint_id, responsible_id: user_id, is_closed: true)
            block.call(tasks)
          end

          def change_state(task_id:, task_state_id:, &block)
            action       = "api/v1.0/tasks/#{task_id}/change_state"
            body_request = {task_state_id: task_state_id}
            response = rest_client[action].post body_request
            task_json = JSON.parse response.body
            dto  = Dto::TaskResponse.new(task_json)
            task = Domain::Entity::Task::Task.new(dto.as_json)
            block.call(task)
          end

          def start task_id, &block
            task = start_stop(task_id, 'play')
            block.call(task)
          end

          def stop task_id, &block
            task = start_stop(task_id, 'pause')
            block.call(task)
          end

          def transfer task_id, responsible_id, team_id, &block
            action   = "api/v1.0/tasks/#{task_id}/transfer"
            body_request = {responsible_id: responsible_id, team_id: team_id}
            response = rest_client[action].post body_request
            task_json = JSON.parse response.body
            dto  = Dto::TaskResponse.new(task_json)
            task = Domain::Entity::Task::Task.new(dto.as_json)
            block.call(task)
          end

          def find_task_description(task_id:, &block)
            action   = "api/v1.0/tasks/#{task_id}/description"
            response = rest_client[action].get
            json = JSON.parse response.body
            dto = Dto::TaskDescriptionResponse.new(json)
            task_description = Domain::Entity::Task::TaskDescription.new(dto.as_json)
            block.call(task_description)
          end

          private
          def tasks where = {}
            action   = "api/v1.0/tasks#{build_query_params(where)}"
            response = rest_client[action].get
            collection = JSON.parse response.body
            collection.collect do |attrs|
              dto = Dto::TaskResponse.new(attrs)
              Domain::Entity::Task::Task.new(dto.as_json)
            end
          end

          def start_stop(task_id, action)
            action       = "api/v1.0/tasks/#{task_id}/#{action}"
            body_request = {id: task_id}
            response  = rest_client[action].post body_request
            task_json = JSON.parse response.body
            dto  = Dto::TaskResponse.new(task_json)
            Domain::Entity::Task::Task.new(dto.as_json)
          end

        end
      end
    end
  end
end
