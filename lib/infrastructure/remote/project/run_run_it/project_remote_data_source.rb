module Infrastructure
  module Remote
    module Project
      module RunRunIt
        class ProjectRemoteDataSource
          include RepositoryBase
          include Domain::Repository::Project::ProjectDataSource

          def find project_id
            action   = "api/v1.0/clients/#{project_id}"
            response = rest_client[action].get
            json = JSON.parse response.body
            dto = Dto::ProjectResponse.new(json)
            Domain::Entity::Project.new(dto.as_json)
          end

          def all &block
            action   = "api/v1.0/clients"
            response = rest_client[action].get
            collection = JSON.parse response.body

            projects = collection.collect do |attrs|
              dto = Dto::ProjectResponse.new(attrs)
              Domain::Entity::Project.new(dto.as_json)
            end

            projects.sort! { |x, y| x.name <=> y.name } if projects.present?
            block.call(projects)
          end

          def all_by_user user_id, &block
            all &block
          end

        end
      end
    end
  end
end
