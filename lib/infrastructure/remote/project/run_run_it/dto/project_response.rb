module Infrastructure
  module Remote
    module Project
      module RunRunIt
        module Dto
          class ProjectResponse
            include Domain::Entity::Base

            attr_reader :id, :name, :is_visible, :project_ids, :project_groups

            def as_json
              super.merge(sprint_ids: project_ids)
            end

          end
        end
      end
    end
  end
end
