module Infrastructure
  module Remote
    module User
      module RunRunIt
        module Dto
          class UserResponse
            include Domain::Entity::Base

            attr_reader :id, :name, :email, :avatar_url, :avatar_large_url,
              :is_master, :is_manager

          end
        end
      end
    end
  end
end
