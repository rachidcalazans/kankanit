module Infrastructure
  module Remote
    module User
      module RunRunIt
        class UserRemoteDataSource
          include RepositoryBase
          include Domain::Repository::User::UserDataSource

          def find_by_email(email)
            users = all

            users.find{ |user| return user if user.email == email }
          end

          def all_by_ids(user_ids)
            users = all

            return [] unless users.present?

            users.select { |user| user_ids.include?(user.id) }
          end

          private
          def all
            action    = "api/v1.0/users"
            response  = rest_client[action].get
            collection = JSON.parse response.body
            collection.collect do |attrs|
              dto = Dto::UserResponse.new(attrs)
              Domain::Entity::User.new(dto.as_json)
            end
          end

        end
      end
    end
  end
end
