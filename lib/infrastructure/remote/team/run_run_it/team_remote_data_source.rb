module Infrastructure
  module Remote
    module Team
      module RunRunIt
        class TeamRemoteDataSource
          include RepositoryBase
          include Domain::Repository::Team::TeamDataSource

          def find(team_id:, &block)
            action    = "api/v1.0/teams/#{team_id}"
            response  = rest_client[action].get
            team_json = JSON.parse response.body
            dto  = Dto::TeamResponse.new(team_json)
            team = Domain::Entity::Team.new(dto.as_json)
            block.call(team)
          end

          def add_comment(task_id:, text:, &block)
            action   = "api/v1.0/tasks/#{task_id}/comments"
            body_request = {text: text}.to_json
            response = rest_client[action].post body_request
            comment_json = JSON.parse response.body
            dto = Dto::CommentResponse.new(comment_json)
            comment = Domain::Entity::Task::Comment.new(dto.as_json)
            block.call(comment)
          end

          private
          def tasks where = {}
            action   = "api/v1.0/tasks#{build_query_params(where)}"
            response = rest_client[action].get
            collection = JSON.parse response.body
            collection.collect do |attrs|
              dto = Dto::TaskResponse.new(attrs)
              Domain::Entity::Task::Task.new(dto.as_json)
            end
          end

          def start_stop(task_id, action)
            action       = "api/v1.0/tasks/#{task_id}/#{action}"
            body_request = {id: task_id}
            response  = rest_client[action].post body_request
            task_json = JSON.parse response.body
            dto  = Dto::TaskResponse.new(task_json)
            Domain::Entity::Task::Task.new(dto.as_json)
          end

        end
      end
    end
  end
end
