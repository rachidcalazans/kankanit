module Infrastructure
  module Remote
    module Team
      module RunRunIt
        module Dto
          class TeamResponse
            include Domain::Entity::Base

            attr_reader :id, :name, :master_user_id, :master_user_name, :user_ids

          end
        end
      end
    end
  end
end

