module Infrastructure
  module Remote
    module Sprint
      module RunRunIt
        class SprintRemoteDataSource
          include RepositoryBase
          include Domain::Repository::Sprint::SprintDataSource

          def find sprint_id
            action   = "api/v1.0/projects/#{sprint_id}"
            response = rest_client[action].get
            json = JSON.parse response.body
            dto = Dto::SprintResponse.new(json)
            Domain::Entity::Sprint.new(dto.as_json)
          end

          def all_by_project(project_id:, &block)
            action   = "api/v1.0/projects"
            response = rest_client[action].get
            collection = JSON.parse response.body


            sprints = collection.collect do |attrs|
              dto = Dto::SprintResponse.new(attrs)
              Domain::Entity::Sprint.new(dto.as_json)
            end

            sprints_selected = sprints.select { |sprint| sprint.project_id == project_id }

            sprints_selected.sort! { |x, y| y.name <=> x.name } if sprints_selected.present?

            block.call(sprints_selected)
          end

          def all_by_project_and_user(project_id:, user_id:, &block)
          end

        end
      end
    end
  end
end
