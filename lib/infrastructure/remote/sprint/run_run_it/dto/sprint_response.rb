module Infrastructure
  module Remote
    module Sprint
      module RunRunIt
        module Dto
          class SprintResponse
            include Domain::Entity::Base

            attr_reader :id, :name, :is_active, :client_id

            def as_json
              super.merge(project_id: client_id)
            end

          end
        end
      end
    end
  end
end
